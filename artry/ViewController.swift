//
//  ViewController.swift
//  artry
//
//  Created by Steve Regis Koalaga on 2019-02-03.
//  Copyright © 2019 Steve Regis Koalaga. All rights reserved.
//

import UIKit
import SceneKit
import ARKit


class ViewController: UIViewController, ARSCNViewDelegate {

    
    @IBOutlet var sceneView: ARSCNView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set the view delegate
        sceneView.delegate = self
        
        // show statistic
        sceneView.showsStatistics = true
    
        // tru to make a label
        
        sceneView.autoenablesDefaultLighting = true
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // create a session configuration
        let configuration = ARImageTrackingConfiguration()
        if let imageToTrack = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: Bundle.main){
        
        configuration.trackingImages = imageToTrack
        configuration.maximumNumberOfTrackedImages = 2
            print("Images Successfully Added")
        }
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    // MARK : - ARSCNView delegate
    
    // detecting the image and display a marker
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        
        // chack the anchor to see if it is an ar anchor
        if let imageAnchor = anchor as? ARImageAnchor {
            
            // creating a plane of the same size as the detected image
            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            // Making the plane transparent
            // plane.firstMaterial?.diffuse.contents = UIColor(white: 1.0, alpha: 0.5)
          
            
            let text = SCNText(string: "Steve", extrusionDepth: 1.0)
            text.firstMaterial?.diffuse.contents = UIColor.black
            let textNode = SCNNode(geometry: text)
            textNode.position = SCNVector3(0.0,0.0 ,0.0)
            textNode.scale = SCNVector3(0.0045,0.0045,0.0045)
            //
            
            
            // Creating a node to display the model
            let planeNode = SCNNode(geometry: plane)
           
            // rotation de 90 pour le plan
            planeNode.eulerAngles.x = -Float.pi/2
            textNode.eulerAngles.x = -Float.pi/3
            node.addChildNode(planeNode)
            node.addChildNode(textNode)
            
            if imageAnchor.referenceImage.name == "card" {
            if let crystalScene = SCNScene(named: "art.scnassets/crystal.scn") {
                if let crystalNode = crystalScene.rootNode.childNodes.first {
                    crystalNode.eulerAngles.x = Float.pi/2
                    planeNode.addChildNode(crystalNode)
                }
                
            }
        }
            if imageAnchor.referenceImage.name == "joker" {
                if let crystalScene = SCNScene(named: "art.scnassets/crystal.scn") {
                    if let crystalNode = crystalScene.rootNode.childNodes.first {
                        crystalNode.eulerAngles.x = Float.pi/2
                        planeNode.addChildNode(crystalNode)
                    }
                    
                }
            }
         
        }
        
        
        return node
    }

}

